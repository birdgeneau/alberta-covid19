#  Copyright (c) 2020. Mike Birdgeneau. All Rights Reserved.

import datetime
import logging
import os
import subprocess

from alberta.data import fetch_ab_data
from alberta.munge import get_last_report
from utils.telegram import telegram_send_file, telegram_bot_sendtext

logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)

mode = os.getenv('TELE_MODE')
if mode == "DEV":
    logger.setLevel(logging.DEBUG)
    logger.info("Running in DEV mode.")
    os.environ['TELE_ID'] = os.getenv('TELE_ID_DEV')
else:
    logger.setLevel(logging.INFO)
    os.environ['TELE_ID'] = os.getenv('TELE_ID_PROD')

# Press the green button in the gutter to run the script.
if __name__ == '__main__':
    date = datetime.date.today().strftime('%Y-%m-%d')

    if os.path.exists(os.path.join('data', 'covid19dataexport.csv')):
        age = (datetime.datetime.now().timestamp() - os.path.getmtime(os.path.join('data', 'covid19dataexport.csv')))
    else:
        age = 3600 * 24

    if age > (3600 * 12.):
        logger.info("Starting fetch.")
        fetch_ab_data()
    else:
        logger.info("Using cached data.")

    logger.info("Calling R script for plot generation.")
    subprocess.call(["/usr/bin/Rscript", "--vanilla", "R/covid_fit.R"])
    subprocess.call(["/usr/bin/Rscript", "--vanilla", "R/ages.R"])
    subprocess.call(["/usr/bin/Rscript", "--vanilla", "R/ab_vs_on.R"])
    subprocess.call(["/usr/bin/Rscript", "--vanilla", "R/cases_by_location.R"])
    subprocess.call(["/usr/bin/Rscript", "--vanilla", "R/waves.R"])

    # Push results to Telegram:
    last_update = get_last_report()

    telegram_bot_sendtext("COVID19 Report for " + datetime.datetime.now().strftime("%B %d, %Y %H:%M"))
    telegram_bot_sendtext("Latest Data for " + last_update['date'] + ": " + str(last_update['cases']) + " new cases.")
    telegram_send_file("output/covid_pred.png", file_caption="Daily New Cases, with Exponential Fit")
    telegram_send_file("output/covid_weekly.png", file_caption="Daily New Cases, by Weekday")
    telegram_send_file("output/covid_age.png", file_caption="Daily New Cases by Age Group")
    telegram_send_file("output/new_cases_by_region.png", file_caption="Daily New Cases by Region")
    telegram_send_file("output/facet_cases_by_region.png", file_caption="Daily New Cases, Faceted by Region")
    telegram_send_file("output/covid_on_ab.png", file_caption="Alberta / Ontario Comparison")        
    telegram_send_file("output/covid_on_ab_normalized.png", file_caption="Alberta / Ontario Comparison (Normalized)")
    telegram_send_file("output/yyc_cases.png", file_caption="New Cases in Calgary")
    telegram_send_file("output/waves.png", file_caption="Ride the Wave")
    telegram_bot_sendtext("For details, or to contribute, visit: https://gitlab.com/mikebirdgeneau/alberta-covid19")
